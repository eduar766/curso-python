# Comparadores
bool_one = 17 < 328 # A es menor (<) que B
bool_two = 100 == (2 * 50) # A es igual (==) que B
bool_three = 19 <= 19 # A es menor e igual (<=) que B
bool_four = -22 >= -18 # A es mayor e igual (>=) que B
bool_five = 99 != 98 + 1 # A es diferente (!=) que B

# Operadores booleanos
"""
     Operadores booleanos
---------------------------
True and True es True
True and False es False
False and True es False
False and False es False

True or True es True
True or False es True
False or True es True
False or False es False

Not True es False
Not False es True

"""
# Estructuras de control.
# if
def using_control_once(): # def nombreDeFuncion()
    if True: # Condicion
        return "Exito 1" # Si se cumple, hace esto

def using_control_again():
    if True:
        return "Exito 2"

print using_control_once()
print using_control_again()

# if-else

answer = "¡No es mas que una herida!"

def caballero_negro():
    if answer == "¡No es mas que una herida!":
        return True
    else:             #else quiere decir que si no se cumple la condicion de if, entonces, se obtine el resultado establecido en else
        return  False      # Asegurate de que esto de como resultado Fals

# elif
def mayor_menor_igual_5(answer):
    if answer > 5:
        return 1
    elif answer < 5:      #elif significa "de lo contrario"    
        return -1
    else:
        return 0

print mayor_menor_igual_5(4)
print mayor_menor_igual_5(5)
print mayor_menor_igual_5(6)
