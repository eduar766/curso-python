# Tipos de Datos
my_ent = 7
my_real = 1.23
mi_bool = True

# Los espacios en blanco son importantes.
def spam():
    huevos = 12
    return = huevos

print spam()

#-------------------------------------------------- Operaciones Matematicas Basicas -----------------------------------
# Suma
count_to = 75 + 25
print count_to

# Exponenciación
huevos = 10 ** 2
print huevos

# Modulo
spam = 3 % 2
print spam

#--------------------------------------------------------------- Cadenas -----------------------------------------------
# Asignando una cadena a una variable se hace mediante ""
Juan = "Hola vida"
var1 = "Hola, \"amigo\" " # Se usa  " \"example\" " para escribir "" como parte de una cadena.

# Acceso por índice
quinta_letra = "MONTY"[4]
print(quinta_letra)

# len() obtiene la cantidad de caracteres de una cadena
loro = "Azul oscuro"
print len(loro)

# lower() convierte todos los caracteres en minusculas.
loro = "Azul oscuro"
print  loro.lower()

# upper() convierte todos los caracteres de una cadena en Mayusculas.
loro = "azul oscuro"
print loro.upper()

# str() convierte en cadena, algo que no es una cadena
pi = 3.14
print str(pi)

# Formateo de Cadenas con %
nombre = raw_input("Cual es tu nombre?")
mision = raw_input("Cual es tu mision?")
color = raw_input("Cual es tu color favorito?")

print "Ah, asi que tu nombre es %s, tu mision es %s, \
y tu color favorito es %s." % (nombre, mision, color)

#------------------------------------------------------------- Hora y Fecha --------------------------------------------
from datetime import datetime
ahora = datetime.now()
print ahora

print ahora.year # Muestra el año
print ahora.month # Muestra el mes
print ahora.day # Muestra el dia

print '%s/%s/%s' % (ahora.month, ahora.day, ahora.year) # Muestra la fecha en este formato xx/xx/xxxx
