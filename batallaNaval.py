from random import randint # Generador aleatorio entre dos valores.

#---------------------------------------------- Generando Tablero --------------------------
tablero = [] # Almacenar el tablero

for i in range(0, 5): # Por cada indice del rango (0, 5) se le va a agregar a la variable tablero, 5 "O"
    tablero.append(["O"] * 5)

def print_tablero(tablero):
    for i in tablero: # Por cada indice de la lista tablero, usar como conector un espacio en blanco " "
        print " ".join(i) # join() conector, o union entre elementos de una lista.
    return i

print_tablero(tablero)

#------------------------------------------ Escondiendo el Barco ------------------------------------------
def fila_aleatoria(tablero): # ubicacion aleatoria en fila con randint()
    return randint(0, len(tablero)-1)

def columna_aleatoria(tablero): # ubicacion aleatoria en columna con randint()
    return randint(0, len(tablero)-1)

barco_fila = fila_aleatoria(tablero)
barco_col = columna_aleatoria(tablero)

#------------------------------------- Buscando el barco -----------------------------------------
adivina_fila = int(raw_input("Adivina Fila: "))
adivina_columna = int(raw_input("Adivina Columna: "))


if adivina_fila == barco_fila:
    print "Felicitaciones, Hundiste mi barco!"
else:
    print "Agua, no tacaste mi barco"

tablero[adivina_fila]="X"
tablero[adivina_columna]="X"

if adivina_fila == barco_fila and adivina_columna == barco_columna:
  print "Felicitaciones! Hundiste mi barco!"
else:
  if (adivina_fila < 0 or adivina_fila > 4) or (adivina_columna < 0 or adivina_columna > 4):
    print "Huy, eso ni siquiera esta en el oceano."
  elif(tablero[adivina_fila][adivina_columna] == "X"):
    print "Ya dijiste esa."
  else:
  	print "No tocaste mi barco!"
  	tablero[adivina_fila][adivina_columna] = "X"
  # ¡Mostrá (turno + 1) acá!
  print_tablero(tablero)
