zoo_animals = [ "mono", "elefante", "tigre", "perro"]

print zoo_animals
if len(zoo_animals) > 3:
	print "El primer animal del zoologico es el " + zoo_animals[0]
	print "El segundo animal del zoologico es el " + zoo_animals[1]
	print "El tercer animal del zoologico es el " + zoo_animals[2]
	print "El cuarto animal del zoologico es el " + str(zoo_animals[3])


# Agregando items a una lista
valija = []
valija.append("anteojosDeSol") # lista.append() agrega un item a una lista, en la u,tima posicion.
valija.append("crema")
valija.append("peine")
valija.append("colonia")

largo_lista = len(valija)

print "Hay items %d en la valija." % (largo_lista)
print valija


# Particionando una lista
valija = ["anteojos", "sombrero", "pasaporte", "computadora", "traje", "zapatos"]

primeros = valija[0:2]  # El primero y segundo ítem (índices cero y uno)
medio = valija[2:4]     # Tercero y cuarto ítem (índices dos y tres)
ultimos = valija[4:6]


# indice
animales = ["oso hormiguero", "tejon", "pato", "emu", "zorro del desierto"]
indice_pato = animales.index("pato") # index() sirve para hallar un valor en la lista "pato"
animales.insert(indice_pato, "cobra") # insert(indice, valor) sirve para encontrar un valor en la lista y reemplazarlo con otro
print animales


# ciclo for
lista_inicial = [5, 3, 1, 2, 4]
lista_cuadrado = []

for numero in lista_inicial:
    lista_cuadrado.append(numero ** 2)
    lista_cuadrado.sort()

print lista_cuadrado

# Diccionarios.

# los diccionarios consisten en un valor asignado a una clave 'Gaviota', etc
residentes = {'Gaviota' : 104, 'Perezoso' : 105, 'Piton de Birmania' : 106}

print residentes['Gaviota'] # Muestra el número de ubicación de la gaviota

print residentes['Perezoso']
print residentes['Piton de Birmania']

# Modificando los diccionarios
menu = {} # Diccionario vacío
menu['Pollo Alfredo'] = 20.50 # Se agrega un nuevo par clave-valor
print menu['Pollo Alfredo']

menu['arroz'] = 10.23
menu['pescado'] = 5
menu['limon'] = 8.04
print "Hay " + str(len(menu)) + " platos en el menu."
print menu

# Cambiando de ideas.
# clave - nombre_animal : valor - ubicación
animales_zoo = { 'Unicornio' : 'Venta de pochoclo',
'Perezoso' : 'Exhibicion del bosque tropical',
'Tigre de Bengala' : 'Casa en la jungla',
'Gaviota' : 'Exhibicion del artico',
'Foca' : 'Exhibicion del artico'}

# Se elimina la entrada 'Unicornio'.
del animales_zoo['Unicornio']
del animales_zoo['Perezoso']
del animales_zoo['Tigre de Bengala']

animales_zoo['Foca'] = 'Hola' # Se le asigna un nuevo valor a la palabra clave foca
print animales_zoo

# eliminando valores de una lista
mochila = ['caramelo', 'cuchillo', 'pan']
mochila.remove('cuchillo') # Se hace con la clase .remove()


# Modificando un diccionario.
inventario = { # <--- Diccionario de nombre inventario
    'oro' : 500,
    'morral' : ['piedra', 'soga', 'piedra preciosa'],
    'mochila' : ['xilofon','cuchillo', 'bolsa de dormir','pan flauta']
}

inventario['bolsa de arpillera'] = ['manzana', 'rubi chiquito', 'osito panda'] # Agregamos una nueva clave al indice, cuyo valor es una lista

inventario['morral'].sort() # Ordenamos una lista dentro del diccioario

inventario['bolsillo'] = ['caracol', 'mora', 'lanas'] # Agregamos otra clave de tipo lista
inventario['mochila'].sort()
inventario['mochila'].remove('cuchillo') # borramos un elemento de una lista, ubicada en un diccionario
inventario['oro'] = inventario['oro'] + 50 # modificamos (sumamos) una cantidad determinada al valor de una clave.

# Usando range() para crear listas
def mi_funcion(x):
    for i in range(0, len(x)): # range(a, b, c) a= inicio b=final c=step
        x[i] = x[i] * 2
    return x

print mi_funcion(range(3)) # Agregá tu range entre paréntesis.
