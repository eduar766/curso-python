from sys import argv

script, filename = argv #pasamos dos argumentos al argv, el script de python y el archivo que vamos a usar.

txt = open (filename) #abrimos el archivo y lo almacenamos en la variable txt

print "Here's your file %r:" % filename
print txt.read() #leemos el archivo y lo mostramos... usamos la opcion .read() en txt porque es la variable que tiene contenida el archivo.

print "Type the filename again:" 
file_again = raw_input("> ") #En esta parte, asignamos el archivo que vamos a tratar a la variable file_again

txt_again = open(file_again) #Acá abrimos el archivo que almacenamos antes

print txt_again.read() #leemos el archivo y lo mostramos en pantalla con print

