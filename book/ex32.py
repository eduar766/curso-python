the_count = [1, 2, 3, 4, 5] # las listas se engloban entre [] y cada elemento se separa por ,
fruits = ['aples', 'oranges', 'pears', 'apricots']
change = [1, 'pennies', 2, 'dimes', 3, 'quarters']

# this first kind of for-loop goes through a list
for number in the_count: # ciclo: for (elemento de iteracion) in (lista): Hace... 
    print "this is count %d" % number
    
# same as above
for fruit in fruits:
    print "A fruit of type: %s" % fruit
    
# also we can go through mixed list too
# notice we have to use %r since we don't know what's in it
for i in change:
    print "I got %r" % i
    
# we can also build list, first start with an empty one
elements = []

# then use the range funtion to do 0 to 5 counts
for i in range(0, 6): # i toma el valor de cada lugar en el rango definido 0, 1, 2, ...
    print "Adding %d to the list." % i
    # append is a function that lists understand
    elements.append(i) # append() agrega el valor de la variable, en este caso i a la lista elements, que esta vacia
    
# now we can print them out too
for i in elements:
    print "Element was: %d" % i
    
