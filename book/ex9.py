#Here's some new strange stuff, remember type it exactly

days = "Mon Tue Wed Thu Fri Sat Sun"
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nAug" # Usamos \n para separar los elementos del string y colocarlos en forma de columna.

print "Here are the days:", days
print "Here are the months:", months

#los """ sirven para escribir un parrafo.
print """ 
    There's something going on here.
    With the three double-quotes
    We'll be able to type as much as we like.
    Even 4 lines if we want, or 5, or 6.
    """
