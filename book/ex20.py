from sys import argv

script, input_file = argv

                                    # Definimos las funciones.
def print_all(f):
    print f.read()  #Leemos el documento como esta naturalmente

def rewind(f):
    f.seek(0) #Se ubica en el principio del documeto.

def print_a_line(line_count, f):
    print line_count, f.readline() #readline() Se encarga de "leer" en que parte del documento estamos y lo muestra en pantalla.
    
                                    # Variables que se pasaran a las funciones.
    
current_file = open(input_file)

print "First let's print the whole file:\n"

print_all(current_file)

print ("Now let's rewind, kind of like a tape.")

rewind(current_file)

print "Let's print three lines:"

current_line = 1
print_a_line(current_line, current_file)

current_line = current_line + 1
print_a_line(current_line, current_file)

current_line = current_line + 1
print_a_line(current_line, current_file)
