my_name = 'Eduardo Saavedra'
my_age = 26 #thats true
my_height = 179 #centimetros
my_weight = 84 #kilogramos
my_eyes = 'Browns'
my_teeth = 'White'
my_hair = 'Black'

print "Let's talk about %s." % my_name #Para hacer llamados dentro de un print de un string, se hace uso de %s
print "He's %d inches tall." % my_height #Para hacer llamados dentro de un print de un dato, se hace uso de %d
print "He's %d pounds heavy." % my_weight
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair." % (my_eyes, my_hair) #cuando son varios datos a los que se tiene que hacer una referencia, se engloban entre parentesis (dato1,dato2) 
print "His teeth are usually %s depending on the coffee." % my_teeth

# this line is tricky, try to get it exactly right
print "If I add %d, %d, and %d I get %d." % (my_age, my_height, my_weight, my_age + my_height + my_weight) 
