tabby_cat = "\tI'm tabbed in." #\t tabulacion
persian_cat = "I'm split\non a line." #\n salto de linea
backslash_cat = "I'm \\ a \\ cat." #Backslash \

fat_cat = """
    I'll do a list:
    \t* Cat food
    \t* Fishes
    \t* Catnip\n\t* Grass
    """

print tabby_cat
print persian_cat
print backslash_cat
print fat_cat
