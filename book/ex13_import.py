from sys import argv #Se pueden agregar pedazos de codigo o scripts en nuestros programas con la funcion import.
                     #argv significa argument variable, esta variable sostiene el valor que le pasamos al script de Python cuando este inicia.

script, first, second, third = argv #Take whatever is in argv, unpack it, and assign it to all of these variables on the left in order.

print "The script is called:", script
print "Your first variable is:", first
print "Your second variable is:", second
print "Your third variable is:", third
