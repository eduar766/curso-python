# this one is like your scripts with argv
def print_two(*args): #def define la estructura de la funcion despues del : se definen variables y lo que hace la funcion.
    arg1, arg2 = args
    print "arg1: %r, arg2: %r" % (arg1, arg2)

# ok, that *args is actually pointless, we can just do this
def print_two_again(arg1, arg2): #arg1,arg2 son los valores que trabajaran en la funcion...
    print "arg1: %r, arg2: %r" % (arg1, arg2)

# this just takes one argument
def print_one(arg1):
    print "arg1: %r" % arg1

# this one takes no arguments
def print_none():
    print "I got nothin'."


print_two("Zed","Shaw") #De esta forma, se le asignan los valores a las funciones: function(arg1,arg2)
print_two_again("Zed","Shaw")
print_one("First!")
print_none()
