# <-- Con este simbolo puedo hacer comentarios.

print "Hello World!" #Miren, soy un comentario gracioso :D
print "Hello Again"
print "I like typing this."
print "This is fun"
print 'Yay! Printing'
print "I'd much rather you 'not'."
print 'I "said" do not touch this.' #Comillas simples pueden contener comillas dobles.
