def add(a, b):
    print "ADDING %d + %d" % (a, b)
    return a + b  #return lo que hace es asignar un valor, a una funcion.

def substract(a, b):
    print "SUBTRACTING %d - %d" % (a, b)
    return a - b

def multiply(a, b):
    print "MULTIPLYING %d * %d" % (a, b)
    return a * b

def divide(a, b):
    print "DIVIDING %d / %d" % (a, b)
    return a / b
    

print "Let's do some math with just functions!"

age = add(30, 5) #Se le asignan valor a las variables de la funcion, luego el resultado de la funcion se almacena en una variable global.
height = substract (78, 4)
weight = multiply(90, 2)
iq = divide(100, 2)

print "Age: %d, Height: %d, weight: %d, IQ: %d" % (age, height, weight, iq)


# A puzzle for the extra credit, type it in anyway.
print "Here is a puzzle"

what = add(age, substract(height, multiply(weight, divide(iq, 2)))) #Aca se demuestra que una funcion puede ser utilizada como argumento de otra funcion.

print "That becomes: ", what, "Can you do it by hand?"
