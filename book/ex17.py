from sys import argv
from os.path import exists #exist retorna 'TRUE' o 'FALSE' dependiendo de lo que se pregunte

script, from_file, to_file = argv

print "Copying from %s to %s" % (from_file, to_file)

# we could do these two on one line, how?
in_file = open(from_file) #abre el archivo from_file y lo almacena en in_file
indata = in_file.read() #lee el archivo de entrada y lo guarde en indata

print "The input file is %d bytes long" % len(indata) #len determina la longitud de la cadena indata

print "Does the output file exist? %r" % exists(to_file) #Pregunta si existe el archivo to_file
print "Ready, hit RETURN to continue, CTRL-C to abort."
raw_input()

out_file = open(to_file, 'w')
out_file.write(indata) #Escribe dentro de to_file, el contenido de from_file

print "Alright, all done."

out_file.close()
in_file.close() #Se cierran los archivos abiertos.
