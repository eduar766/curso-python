usuario = {'nombre': 'X', 'ciudad': 'X', 'direccion': 'x', 'telefono': 041525 }
destino = {'Madrid': 1, 'NY': 2, 'Londres': 3, 'Tokio': 4 }

def start():
    presentacion = """
    Bienvenido a nuestro sistema de Calculo de viaje. Por favor suministra la siguiente informacion
    personal.\n
    """
    print presentacion
    print "-----------------------------------------------------------------"
    usuario['nombre'] = raw_input("Nombre y Apellido >> ")
    usuario['ciudad'] = raw_input("Ciudad donde vives " + usuario['nombre'] + " >> ")
    usuario['direccion'] = raw_input("Direccion >> ")
    usuario['telefono'] = raw_input("Proporciona un numero de telefono para contactarte >> ")

    return usuario

def ciudadDestino():
    presentacion = """
    Ahora seleccione la ciudad a la que desea viajar\n
    1.- Madrid
    2.- New York
    3.- Londres
    4.- Tokio\n
    """
    print presentacion
    print "-----------------------------------------------------------------"

    ciudad = raw_input("Selecciona (el numero) de la ciudad que deseas visitar >> ")
    ciudad = int(ciudad)

    if ciudad == 1:
        ciudadPartida(ciudad)
        return ciudad
    elif ciudad == 2:
        ciudadPartida(ciudad)
        return ciudad
    elif ciudad == 3:
        ciudadPartida(ciudad)
        return ciudad
    elif ciudad == 4:
        ciudadPartida(ciudad)
        return ciudad
    else:
        ciudadPartida(ciudad)
        return ciudad

def ciudadPartida(x):
    intro = """
    Muy bien, ahora escoge la ciudad desde donde vas a partir:\n
    1.- Caracas
    2.- Buenos Aires
    3.- Bogota\n
    """
    print intro

    y = raw_input("Selecciona (el numero) de la ciudad desde donde partiras >> ")
    y =int(y)

    if x == 1:
        print "Elegiste viajar para Madrid"
        if y == 1:
            print "Desde Caracas"
            destino['Madrid'] = 3500
            diasDeViaje(destino['Madrid'])
            return destino['Madrid']
        elif y == 2:
            print "Desde Buenos Aires"
            destino['Madrid'] = 4500
            diasDeViaje(destino['Madrid'])
            return destino['Madrid']
        elif y == 3:
            print "Desde Bogota"
            destino['Madrid'] = 5500
            diasDeViaje(destino['Madrid'])
            return destino['Madrid']
        else:
            print "No es una opcion valida"
        return y
    elif x == 2:
        print "Elegiste viajar para New York"
        if y == 1:
            print "Desde Caracas"
            destino['NY'] = 2500
            diasDeViaje(destino['NY'])
            return destino['Madrid']
        elif y == 2:
            print "Desde Buenos Aires"
            destino['NY'] = 3800
            diasDeViaje(destino['NY'])
            return destino['Madrid']
        elif y == 3:
            print "Desde Bogota"
            destino['NY'] = 3000
            diasDeViaje(destino['NY'])
            return destino['Madrid']
        else:
            print "No es una opcion valida"
        return y
    elif x == 3:
        print "Elegiste viajar para Londres"
        if y == 1:
            print "Desde Caracas"
            destino['Londres'] = 5000
            diasDeViaje(destino['Londres'])
            return destino['Madrid']
        elif y == 2:
            print "Desde Buenos Aires"
            destino['Londres'] = 5400
            diasDeViaje(destino['Londres'])
            return destino['Madrid']
        elif y == 3:
            print "Desde Bogota"
            destino['Londres'] = 6800
            diasDeViaje(destino['Londres'])
            return destino['Madrid']
        else:
            print "No es una opcion valida"
        return y
    elif x == 4:
        print "Elegiste viajar para Tokio"
        if y == 1:
            print "Desde Caracas"
            destino['Tokio'] = 8000
            diasDeViaje(destino['Tokio'])
            return destino['Madrid']
        elif y == 2:
            print "Desde Buenos Aires"
            destino['Tokio'] = 8200
            diasDeViaje(destino['Tokio'])
            return destino['Madrid']
        elif y == 3:
            print "Desde Bogota"
            destino['Tokio'] = 8500
            diasDeViaje(destino['Tokio'])
            return destino['Madrid']
        else:
            print "No es una opcion valida"
        return y
    else:
        print "Escoge una opcion valida por favor..."
        start()

    return x

def diasDeViaje(w):

    global dias
    dias = raw_input("Cuantos dias vas a pasar de viaje? >> ")
    dias = int(dias)

    global vdias
    vdias = dias * w

    print "En viaje, gastaras: %r$" % vdias


start()
ciudadDestino()

print "Muy bien " + usuario['nombre'] + " Ahora, deseas que te proporcionemos una estadia?"

siNO = raw_input("Escribe SI o NO >> ")
siNO = siNO.lower()

if siNO == "no":
    print "Excelente, entonces suponemos que tienes estadia..."
    exit()
elif siNO == "si":
    print """
    Bien, ahora debes escoger el nivel de hotel que deseas:\n
    a) Cinco Estrellas (560$ por noche)
    b) Mediana Calidad (350$ por noche)
    c) Mala muerte (75$ por noche)\n
    """

    estadia = raw_input("Selecciona la opcion deseada (letra) >> ")
    global vestadia

    if estadia == "a":
        vestadia = dias * 560
        print "El valor de tu estadia es de %r$" % vestadia
    elif estadia == "b":
        vestadia = dias * 350
        print "El valor de tu estadia es de %r$" % vestadia
    elif estadia == "c":
        vestadia = dias * 75
        print "El valor de tu estadia es de %r$" % vestadia
    else:
        print "porque no eliges una opcion correcta?"
        start()

else:
    print "Solo debes escribir SI o NO... No es muy dificil"
    start()

def exit():
    total = vestadia + vdias
    print "Excelente " + usuario['nombre'] + " el total que vas a gastar es de %r$" % total
    print "Deseas que enviemos la informacion a tu casa ubicada en la ciudad de "+ usuario['ciudad'] + " " + usuario['direccion'] + "?"
    print "En todo caso, te estaremos llamando al " + usuario['telefono'] + " para concretar los detalles de la reservacion"


exit()
