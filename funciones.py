# Estructura
def spam(): # Encabezado
    """Muestra con un print la palabra Huevos""" # Descripción de lo que hace la función
    print "Huevos!" # Codigo de ejecución.

spam() # Llamado a ejecucion de la funcion.

#-------------------------------------------------------------------------------------------------------------------
#  variables y argumentos dentro de las funciones.
def cuadrado(n): # n es una variable que se le pasa de manera externa a esta funcion
    """Devuelve el cuadrado de un numero."""
    squared = n**2
    print "%d al cuadrado es %d." % (n, squared)
    return cuadrado

cuadrado(10) # el 10 es el argumento que tendra como valor la variable "n" en la funcion cuadrado.

def potencia(base, exponente):  # parametros base y exponente
    resultado = base**exponente
    print "%d a la %d potencia es %d." % (base, exponente, resultado)

potencia(2, 5)

# funciones dentro de otras funciones
def amor_con_amor(n):
    return n + 1

def se_paga(n):
    return amor_con_amor(n) + 2

# Ejemplo:
def cubo(numero):
    return numero*numero*numero

def por_tres(numero):
    if (numero % 3 == 0):
        return cubo(numero)
    else:
        return False

# Modulos e importaciones
from math import * # importa todas las funciones de la libreria math
from math import sqrt # importa solo la funcion sqrt de math.

import math            # Importa el módulo math
everything = dir(math) # Establece todo a una lista de cosas desde math
print everything       # ¡Lo muestra todo!

# Otras funciones
maximum = max(9, 58, 14, 69) # evalua todas los argumentos y escoje el de mayor valor.
minimum = min(1, 58, 32, 441, 0) # evalua todos los argumentos y escoje el de menor valor.
absolute = abs(-42) # abs solo recibe una variable y calcula el valor absoluto de la misma.
print type(42) # Muestra el tipo de dato de la variable str, int, string, etc
